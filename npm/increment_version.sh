
function npm.increment_version() {
  # The two versions should now be the same...
  OLD_VERSION_FROM_PKG="$(npm ls . --long=true --parseable=true)" || true
  OLD_VERSION_FROM_GIT="$(git describe --abbrev=0 --tag)"

  echo " *** Old version from package.json: $OLD_VERSION_FROM_PKG"
  echo " *** Old version from git: $OLD_VERSION_FROM_GIT"

  unset NEW_VERSION
  if [[ $BRANCH == release/* ]] ; then
    echo ' *** This is a release branch, generating rc version'
      NEW_VERSION="$(npm version prerelease --preid=rc --no-git-tag-version)"
  elif [[ $BRANCH == 'develop' ]] ; then
    echo ' *** This is branch develop, generating new patch version'
    NEW_VERSION="$(npm version patch --no-git-tag-version)"
  fi

  if declare -p NEW_VERSION &> /dev/null; then
    echo ' *** Executing git tagging and push to repository'

    # package.json is modified by previous npm commands
    git add package*
    # --no-verify is needed by husky
    git commit -m "New version: $NEW_VERSION" --no-verify
    # Adding the tag to the previous commit
    git tag "$NEW_VERSION"

    git push --no-verify
    git push --tags --no-verify
    echo " *** Pushed new version tag: $NEW_VERSION"
  fi
}
