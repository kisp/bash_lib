import require_package


function say_notification() {
  local lang="${1:-mb-us1}"
  # This is a pretty good synthetizer
  require_package mbrola dbus-monitor

  # Todo introduce negative filter on starting ':' or the string urgency
  dbus-monitor --session interface='org.freedesktop.Notifications' |
  \grep -oe 'string.*' --line-buffered |
  stdbuf -oL cut -d '"' -f 2 |
  espeak -a 100 -p 25 -s 130 -g 1 -v "$lang"
}

